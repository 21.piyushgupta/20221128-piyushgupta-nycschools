//
//  HomeTableViewController.swift
//  NYC
//
//  Created by Piyush Gupta on 11/26/22.
//

import UIKit
import Foundation

class HomeViewController: UIViewController   {
    
    
    lazy var viewModel: HomeViewModel = HomeViewModel(delegate: self)
    
    @IBOutlet var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadSchoolData()
        tableView.separatorStyle = .none
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailViewController = segue.destination as? DetailViewController else{
            return
        }
        if let selectedRowIndexPath = tableView.indexPathForSelectedRow {
            detailViewController.viewModel = DetailViewModel(selectedNycSchool: viewModel.nycSchools[selectedRowIndexPath.row], delegate: detailViewController.self)
        }
    }

}

// MARK: - Table View implementation
extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    /**
    *  Adding two section, one with NYC School List and one for a Loading Indicator.
    */
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    /**
    *  For Section 1 i.e. List cell, number of rows will be equal to number of schools fetched from the service.
    *  For Section 2 i.e. there is only 1 cell, for Loading Indicator.
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return viewModel.nycSchools.count
        } else {
            return 1
        }
    }
    
    /**
    *  Setting Loading Indicator i.e. Section 2 height only as 44, it should be suffice height on all devices
    *  For Section 1, calculate automatic height as data might be different.
    */
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 1{
            return 44   
        } else {
            return UITableView.automaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as? HomeTableViewCell else {
                fatalError()
            }
            
            cell.lblSchoolName.text = viewModel.nycSchools[indexPath.row].name
            cell.lblAddress.text = viewModel.nycSchools[indexPath.row].primaryAddress
            cell.lblOverview.text = viewModel.nycSchools[indexPath.row].overview
            cell.btnMakeCall.setTitle(viewModel.nycSchools[indexPath.row].phoneNumber, for: .normal)
            cell.btnGetDirections.setTitle("Get Directions", for: .normal)
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ActivityIndicatorCell", for: indexPath) as? ActivityIndicatorCell else {
                fatalError()
            }
            if viewModel.isDataLoading {
                cell.activityIndicator.startAnimating()
            } else {
                cell.activityIndicator.stopAnimating()
            }
            return cell
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (((scrollView.contentOffset.y + scrollView.frame.size.height) > scrollView.contentSize.height ) && !viewModel.isDataLoading){
            self.tableView.reloadData()
            viewModel.loadMore()
        }
    }
    
}


// MARK: - View Model implementation

extension HomeViewController: HomeViewModelDelegate{

    /**
    * reload Table Data when a succesful response is received.
    */
    func reloadTableView() {
        DispatchQueue.main.async {[weak self] in
            self?.tableView.reloadData()
        }
    }
    
    /**
    * Show an alert whenever a service get failed.
    * Please note, all the hardcoded strings need to set with Localization in future enhancement.
    */
    func showErrorMessage() {
        DispatchQueue.main.async { [weak self] in
            //calling tableview reload so that loading indicator will stop animating.
            self?.tableView.reloadData()
            //Show Alert for Service Failure.
            let alertController = UIAlertController(title: "Info", message: "We are having some issue while fetching the list of Schools, Please try again.", preferredStyle: .alert)
            let actionOK = UIAlertAction(title: "OK", style: .default)
            alertController.addAction(actionOK)
            self?.present(alertController, animated: true)
        }
    }


}
