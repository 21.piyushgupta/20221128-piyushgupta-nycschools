//
//  HomeViewModel.swift
//  NYC
//
//  Created by Piyush Gupta on 11/26/22.
//

import Foundation
import Combine


protocol HomeViewModelProtocol: AnyObject{
    func loadSchoolData() -> Void
    func loadMore() -> Void
    var isDataLoading: Bool {get}
    var offset: Int {get}
    var limit: Int {get}
    var nycSchools: [NYCSchool] {get}
}

protocol HomeViewModelDelegate: AnyObject{
    func reloadTableView() -> Void
    func showErrorMessage() -> Void
}

class HomeViewModel: NSObject, HomeViewModelProtocol{
    
    var isDataLoading: Bool = false
    
    var offset: Int = 0
    
    var limit: Int = 20
    
    var nycSchools: [NYCSchool] = [NYCSchool]()
    
    weak var delegate: HomeViewModelDelegate?
    
    init(delegate: HomeViewModelDelegate) {
        self.delegate = delegate
    }
    
    func loadSchoolData() {
        if(!self.isDataLoading){
            self.isDataLoading = true
            NetworkManager.shared.getData(endpoint: .list, queryItems: [URLQueryItem(name: "$limit", value: "\(limit)"), URLQueryItem(name: "$offset", value: "\(offset)")], type: [NYCSchool].self) {[weak self] schools, error in
                if let self = self{
                    self.isDataLoading = false
                    if error != nil{
                        self.delegate?.showErrorMessage()
                    }
                    if let schools = schools{
                        self.nycSchools = schools
                        self.delegate?.reloadTableView()
                    }
                }
            }
        }
    }
    
    func loadMore(){
        offset = self.nycSchools.count
        limit = offset + 20
        loadSchoolData()
    }
}
