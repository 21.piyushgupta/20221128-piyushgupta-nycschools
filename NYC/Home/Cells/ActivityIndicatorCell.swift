//
//  LoadingTableViewCell.swift
//  NYC
//
//  Created by Piyush Gupta on 11/26/22.
//

import UIKit

class ActivityIndicatorCell: UITableViewCell {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
