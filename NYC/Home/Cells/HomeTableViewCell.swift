//
//  HomeTableViewCell.swift
//  NYC
//
//  Created by Piyush Gupta on 11/26/22.
//

import UIKit

class HomeTableViewCell: UITableViewCell {
    
    @IBOutlet var lblSchoolName: UILabel!
    @IBOutlet var lblOverview: UILabel!
    @IBOutlet var lblAddress: UILabel!
    
    @IBOutlet var btnMakeCall: UIButton!
    @IBOutlet var btnGetDirections: UIButton!
    
    
    @IBOutlet var cellBackgroundView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        btnMakeCall.setNonLeadingContentInsets()
        btnGetDirections.setNonLeadingContentInsets()
        cellBackgroundView.backgroundColor = UIColor(patternImage: UIImage(named: "Background")!)
        cellBackgroundView.layer.borderColor = UIColor(named: "ValueText")!.cgColor
        cellBackgroundView.layer.borderWidth = 1.0
        cellBackgroundView.layer.cornerRadius = 6.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
