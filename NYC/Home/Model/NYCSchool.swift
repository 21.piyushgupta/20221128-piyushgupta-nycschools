//
//  Schools.swift
//  NYC
//
//  Created by Piyush Gupta on 11/26/22.
//

import Foundation

struct NYCSchool: Decodable{
    
    let name: String
    let Id: String
    let phoneNumber: String?
    let faxNumber: String?
    let email: String?
    let website: String?
    let primaryAddressLine1: String?
    let city: String?
    let zip: String?
    let stateCode: String?
    let overview: String?
    
    let academicopportunities1: String?
    let academicopportunities2: String?
    
    let requirement1_1: String?
    let requirement2_1: String?
    let requirement3_1: String?
    let requirement4_1: String?
    let requirement5_1: String?
    
    let admissionspriority11: String?
    let admissionspriority21: String?
    let admissionspriority31: String?
    
    
    var primaryAddress: String{
        var address = ""
        if let primaryAddressLine1 = primaryAddressLine1 {
            address += primaryAddressLine1
        }
        if let city = city  {
            address += " " + city
        }
        if let stateCode = stateCode  {
            address += " " + stateCode
        }
        if let zip = zip  {
            address += " " + zip
        }
        return address
    }
    
    var admissionsPriorities: String{
        var priorities = ""
        if let admissionspriority11 = admissionspriority11  {
            priorities += admissionspriority11
        }
        if let admissionspriority21 = admissionspriority21  {
            priorities += ", " + admissionspriority21
        }
        if let admissionspriority31 = admissionspriority31  {
            priorities += ", " + admissionspriority31
        }
        if(priorities != ""){
            return priorities.replacingOccurrences(of: ", ", with: "\n")
        } else{
            return priorities
        }
    }
    
    var academicOpportunities: String{
        var opportunities = ""
        if let academicopportunities1 = academicopportunities1  {
            opportunities += academicopportunities1
        }
        if let academicopportunities2 = academicopportunities2  {
            opportunities += ", " + academicopportunities2
        }
        if(opportunities != ""){
            return opportunities.replacingOccurrences(of: ", ", with: "\n")
        } else{
            return opportunities
        }
    }
    
    var admissionRequirements: String  {
        var requirements = ""
        if let requirement1_1 = requirement1_1  {
            requirements += requirement1_1
        }
        if let requirement2_1 = requirement2_1  {
            requirements += ", " + requirement2_1
        }
        if let requirement3_1 = requirement3_1  {
            requirements += ", " + requirement3_1
        }
        if let requirement4_1 = requirement4_1  {
            requirements += ", " + requirement4_1
        }
        if let requirement5_1 = requirement5_1  {
            requirements += ", " + requirement5_1
        }
        if(requirements != ""){
            return requirements.replacingOccurrences(of: ", ", with: "\n")
        } else{
            return requirements
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case school_name
        case dbn
        case phone_number
        case fax_number
        case school_email
        case website
        case primary_address_line_1
        case city
        case zip
        case state_code
        case overview_paragraph
        //Academic Opportunities
        case academicopportunities1
        case academicopportunities2
        //Admission Requirements
        case requirement1_1
        case requirement2_1
        case requirement3_1
        case requirement4_1
        case requirement5_1
        //Admission Priorities
        case admissionspriority11
        case admissionspriority21
        case admissionspriority31
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decode(String.self, forKey: .school_name)
        self.Id = try container.decode(String.self, forKey: .dbn)
        self.phoneNumber = try container.decodeIfPresent(String.self, forKey: .phone_number)
        self.faxNumber = try container.decodeIfPresent(String.self, forKey: .fax_number)
        self.email = try container.decodeIfPresent(String.self, forKey: .school_email)
        self.website = try container.decodeIfPresent(String.self, forKey: .website)
        self.primaryAddressLine1 = try container.decodeIfPresent(String.self, forKey: .primary_address_line_1)
        self.city = try container.decodeIfPresent(String.self, forKey: .city)
        self.zip = try container.decodeIfPresent(String.self, forKey: .zip)
        self.stateCode = try container.decodeIfPresent(String.self, forKey: .state_code)
        self.overview = try container.decodeIfPresent(String.self, forKey: .overview_paragraph)
        self.academicopportunities1 = try container.decodeIfPresent(String.self, forKey: .academicopportunities1)
        self.academicopportunities2 = try container.decodeIfPresent(String.self, forKey: .academicopportunities2)
        self.requirement1_1 = try container.decodeIfPresent(String.self, forKey: .requirement1_1)
        self.requirement2_1 = try container.decodeIfPresent(String.self, forKey: .requirement2_1)
        self.requirement3_1 = try container.decodeIfPresent(String.self, forKey: .requirement3_1)
        self.requirement4_1 = try container.decodeIfPresent(String.self, forKey: .requirement4_1)
        self.requirement5_1 = try container.decodeIfPresent(String.self, forKey: .requirement5_1)
        self.admissionspriority11 = try container.decodeIfPresent(String.self, forKey: .admissionspriority11)
        self.admissionspriority21 = try container.decodeIfPresent(String.self, forKey: .admissionspriority21)
        self.admissionspriority31 = try container.decodeIfPresent(String.self, forKey: .admissionspriority31)
    }
    
}
