//
//  OpenUrlService.swift
//  NYC
//
//  Created by Piyush Gupta on 11/27/22.
//

import Foundation
import UIKit

struct OpenUrlService{
    
    static let shared = OpenUrlService()
    
    func makeACall(_ phoneNumber: String){
        if let url = URL(string: "tel://\(phoneNumber)") {
            openURL(url: url)
        }
    }
    
    func navigateToWebsite(_ website: String){
        if let url = URL(string: website.starts(with: "https") || website.starts(with: "http") ? website  : "https://\(website)"){
            openURL(url: url)
        }
    }
    
    func sendMail(_ email: String){
        if let url = URL(string: "mailto:\(email)"){
            openURL(url: url)
        }
    }
    
    private func openURL(url: URL){
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    private init(){
        
    }
    
    
}
