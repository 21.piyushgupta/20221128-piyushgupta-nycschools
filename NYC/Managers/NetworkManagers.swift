//
//  NetworkManagers.swift
//  NYC
//
//  Created by Piyush Gupta on 11/26/22.
//

import Foundation

protocol NetworkManagerService {
    func getData<T: Decodable>(endpoint: Endpoint, queryItems: [URLQueryItem]?, type: T.Type, completionHandler: @escaping (T?, Error?) -> Void)
    func cancel()
}

enum Endpoint: String {
    case list = "s3k6-pzi2.json"
    case detail = "f9bf-2cp4.json"
}

enum NetworkError: Error {
    case invalidURL
    case responseError
    case unknown
}

extension NetworkError: LocalizedError {
    var errorDescription: String? {
        switch self {
        case .invalidURL:
            return NSLocalizedString("Invalid URL", comment: "Invalid URL")
        case .responseError:
            return NSLocalizedString("Unexpected status code", comment: "Invalid response")
        case .unknown:
            return NSLocalizedString("Unknown error", comment: "Unknown error")
        }
    }
}

class NetworkManager: NSObject, NetworkManagerService, URLSessionDelegate {
    static let shared = NetworkManager()
    
    private let baseURL = "https://data.cityofnewyork.us/resource/"
    private var task:URLSessionTask?
    
    private lazy var session: URLSession = {
        let configuration = URLSessionConfiguration.ephemeral
        configuration.timeoutIntervalForRequest = 10.0
        return URLSession(configuration: configuration, delegate: self, delegateQueue: OperationQueue.main)
    }()
    
    private override init() {

    }
    
    func getData<T: Decodable>(endpoint: Endpoint, queryItems: [URLQueryItem]? = nil,type: T.Type, completionHandler: @escaping (T?, Error?) -> Void) where T : Decodable {
        
        //generate url
        guard var components = URLComponents(string: self.baseURL.appending(endpoint.rawValue)) else {
            completionHandler(nil, NetworkError.invalidURL)
            return
        }
        if(queryItems != nil){
            components.queryItems = queryItems
        }
            
        guard let url = components.url else {
            completionHandler(nil, NetworkError.invalidURL)
            return
        }
        //generate request
        let request = URLRequest(url: url)
        print(request)
        task = session.dataTask(with: request) { (data, response, error) in
            guard let httpResponse = response as? HTTPURLResponse, 200...299 ~= httpResponse.statusCode else {
                completionHandler(nil, NetworkError.responseError)
                return
            }
            if error != nil  {
                completionHandler(nil, error)
                return
            }
            guard let data = data else {
                completionHandler(nil, NetworkError.unknown)
                return
            }
            self.parseJSON(data: data, type:T.self, completionHandler: completionHandler)
        }
        task?.resume()
    }
    
    func cancel() {
        task?.cancel()
    }
    
    private func parseJSON<T: Decodable>(data: Data, type: T.Type, completionHandler: @escaping (T?, Error?) -> Void){
        do{
            let response = try JSONDecoder().decode(type, from: data)
            completionHandler(response, nil)
        } catch{
            completionHandler(nil, error)
        }
    }
}
