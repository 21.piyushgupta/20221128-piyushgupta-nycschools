//
//  UIButton+Extension.swift
//  NYC
//
//  Created by Piyush Gupta on 11/26/22.
//

import UIKit

extension UIButton{
    
    func setNonLeadingContentInsets(){
        self.configuration?.contentInsets = NSDirectionalEdgeInsets(top: 8.0, leading: 0, bottom: 8.0, trailing: 8.0)
    }
    
}
