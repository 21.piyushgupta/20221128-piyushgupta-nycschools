//
//  UIScrollView.swift
//  NYC
//
//  Created by Piyush Gupta on 11/26/22.
//

import UIKit

extension UIScrollView {
    func updateContentView() {
        contentSize.height = subviews.sorted(by: { $0.frame.maxY < $1.frame.maxY }).last?.frame.maxY ?? contentSize.height
    }
}
