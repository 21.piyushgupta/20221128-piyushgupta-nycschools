//
//  LoadingIndicatorView.swift
//  NYC
//
//  Created by Piyush Gupta on 11/27/22.
//

import UIKit

class LoadingIndicatorView: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblErrorMsg: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    func initSubviews() {
        let nib = UINib(nibName: "LoadingIndicatorView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    func showOrHideLoadingIndicator(shouldShowLoadingIndicator: Bool){
        DispatchQueue.main.async { [weak self] in
            if let self = self{
                self.activityIndicator.isHidden = !shouldShowLoadingIndicator
                self.lblErrorMsg.isHidden = shouldShowLoadingIndicator
                shouldShowLoadingIndicator ? self.activityIndicator.startAnimating() : self.activityIndicator.stopAnimating()
            }
        }
    }

}
