//
//  SATScoreView.swift
//  NYC
//
//  Created by Piyush Gupta on 11/27/22.
//

import UIKit

class SATScoreView: UIView {
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblWritingScore: UILabel!
    @IBOutlet weak var lblReadingScore: UILabel!
    @IBOutlet weak var lblMathScore: UILabel!
    @IBOutlet weak var lblTotalStudents: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    func initSubviews() {
        let nib = UINib(nibName: "SATScoreView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    func setScoreView(score: SATScore){
        lblTotalStudents.text = score.totalStudents
        lblMathScore.text = score.avgMathScore
        lblReadingScore.text = score.avgReadingScore
        lblWritingScore.text = score.avgWritingScore
    }

}
