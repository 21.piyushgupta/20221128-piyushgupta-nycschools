//
//  OtherInformationView.swift
//  NYC
//
//  Created by Piyush Gupta on 11/27/22.
//

import UIKit

class OtherInformationView: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var lblAcademicOpportunities: UILabel!
    @IBOutlet weak var lblAdmissionRequirements: UILabel!
    @IBOutlet weak var lblAdmissionPriorities: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }

    func initSubviews() {
        let nib = UINib(nibName: "OtherInformationView", bundle: nil)
        nib.instantiate(withOwner: self, options: nil)
        contentView.frame = bounds
        addSubview(contentView)
    }

    func fillOtherInformationSection(school: NYCSchool){
        fillInformation(forLbl: lblAcademicOpportunities, withMessageAs: school.academicOpportunities)
        fillInformation(forLbl: lblAdmissionPriorities, withMessageAs: school.admissionsPriorities)
        fillInformation(forLbl: lblAdmissionRequirements, withMessageAs: school.admissionRequirements)
    }
    
    private func fillInformation(forLbl lbl: UILabel, withMessageAs msg: String){
        lbl.text = msg.isEmpty ? "No Information Available" : msg
    }

}
