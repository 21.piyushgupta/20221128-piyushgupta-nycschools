//
//  DetailViewController.swift
//  NYC
//
//  Created by Piyush Gupta on 11/26/22.
// Detail View is subdivided into three part
// 1. TopView -> Blue banner view containing information about School and its contact detail
// 2. Middle View -> Contains description about the school and segment to swicth between  other information
// view and SAT Score View.
// 3. Bottom View -> A Stack show or hide SAT Score View or Other Information view based on selected segment.
//

import UIKit

class DetailViewController: UIViewController {
    
    var viewModel: DetailViewModel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    //Top View
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var phoneNumberButton: UIButton!
    @IBOutlet weak var faxNumberButton: UIButton!
    @IBOutlet weak var emailToButton: UIButton!
    @IBOutlet weak var navigateToWebsite: UIButton!
    
    //Middle view
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    //Bottom View
    @IBOutlet weak var otherInformationView: OtherInformationView!
    @IBOutlet weak var scoreView: SATScoreView!
    @IBOutlet weak var loadingIndicatorView: LoadingIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.loadSATScores()
        fillDetailInformation()
        setUpUIWidgetsOnLoad()
    }
    
    private func setUpUIWidgetsOnLoad(){
        overviewLabel.setLineSpacing(lineSpacing: 1.5, lineHeightMultiple: 1.2)
        
        emailToButton.setNonLeadingContentInsets()
        faxNumberButton.setNonLeadingContentInsets()
        phoneNumberButton.setNonLeadingContentInsets()
        navigateToWebsite.setNonLeadingContentInsets()
        
        segmentedControl.backgroundColor = .white
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor(named: "AccentColor")!], for: .selected)
        segmentedControl.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.black], for: .normal)
        
        self.view.layoutIfNeeded()
        scrollView.updateContentView()
        
    }
    
    override func viewDidLayoutSubviews() {
        self.view.layoutIfNeeded()
        scrollView.updateContentView()
    }
    
    
    // MARK: - IB ACTIONS
    
    @IBAction func onSegmentChange(_ sender: Any) {
        otherInformationView.isHidden = segmentedControl.selectedSegmentIndex == 0
        //if SAT Score is fetched successfully, hide Loading Indicator view, In this case
        // on Segment Change will happen between Other Information view and Score View #If
        //If SAT Score is unavailable for any reason, or is getting loaded, hide Score view, In this case
        // on Segment Change will happen between Other Information view and Loading Indicator View #else
        if viewModel.satScore != nil {
            scoreView.isHidden = segmentedControl.selectedSegmentIndex != 0
            loadingIndicatorView.isHidden = true
        } else {
            loadingIndicatorView.isHidden = segmentedControl.selectedSegmentIndex != 0
            scoreView.isHidden = true
        }
    }

    @IBAction func mailTo(_ sender: Any) {
        viewModel.mailTo()
    }

    @IBAction func navigateToWebsite(_ sender: Any) {
        viewModel.navigateToWebsite()
    }
    
    @IBAction func makeACall(_ sender: Any) {
        viewModel.makeACall()
    }
    
    // MARK: - FILL DETAIL INFORMATION
    
    private func fillDetailInformation(){
        nameLabel.text = self.viewModel.selectedNycSchool.name
        addressLabel.text = self.viewModel.selectedNycSchool.primaryAddress
        phoneNumberButton.setTitle(self.viewModel.selectedNycSchool.phoneNumber, for: .normal)
        faxNumberButton.setTitle(self.viewModel.selectedNycSchool.faxNumber, for: .normal)
        emailToButton.setTitle(self.viewModel.selectedNycSchool.email, for: .normal)
        navigateToWebsite.setTitle(self.viewModel.selectedNycSchool.website, for: .normal)
        overviewLabel.text = self.viewModel.selectedNycSchool.overview
        otherInformationView.fillOtherInformationSection(school: self.viewModel.selectedNycSchool)
    }
    
    private func updateSATScoreUI(){
        if let satScore = self.viewModel.satScore{
            //when SAT Score are available, Hide Loading Indicator View and Show Score View only.
            scoreView.setScoreView(score: satScore)
            scoreView.isHidden = false
            loadingIndicatorView.isHidden = true
        }
    }
}

// MARK: - Detail View Model Delegate
extension DetailViewController: DetailViewModelDelegate{
    func showOrHideLoadingIndicator(shouldShowLoadingIndicator: Bool){
        DispatchQueue.main.async { [weak self] in
            if let self = self{
                self.loadingIndicatorView.showOrHideLoadingIndicator(shouldShowLoadingIndicator: shouldShowLoadingIndicator)
                if(!shouldShowLoadingIndicator){
                    self.updateSATScoreUI()
                }
            }
        }
    }
}



