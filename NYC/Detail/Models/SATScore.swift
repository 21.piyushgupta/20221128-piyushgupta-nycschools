//
//  SATScore.swift
//  NYC
//
//  Created by Piyush Gupta on 11/26/22.
//

import Foundation

struct SATScore: Decodable{
    
    let schoolName: String
    let Id: String
    let totalStudents: String?
    let avgWritingScore: String?
    let avgReadingScore: String?
    let avgMathScore: String?
    
    
    enum CodingKeys: CodingKey {
        case school_name
        case dbn
        case num_of_sat_test_takers
        case sat_critical_reading_avg_score
        case sat_math_avg_score
        case sat_writing_avg_score
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.schoolName = try container.decode(String.self, forKey: .school_name)
        self.Id = try container.decode(String.self, forKey: .dbn)
        self.totalStudents = try container.decodeIfPresent(String.self, forKey: .num_of_sat_test_takers)
        self.avgWritingScore = try container.decodeIfPresent(String.self, forKey: .sat_writing_avg_score)
        self.avgReadingScore = try container.decodeIfPresent(String.self, forKey: .sat_critical_reading_avg_score)
        self.avgMathScore = try container.decodeIfPresent(String.self, forKey: .sat_math_avg_score)
    }
    
}
