//
//  DetailViewModel.swift
//  NYC
//
//  Created by Piyush Gupta on 11/27/22.
//

import Foundation

protocol DetailViewModelDelegate: AnyObject{
    func showOrHideLoadingIndicator(shouldShowLoadingIndicator: Bool)
}

protocol DetailViewModelProtocol: AnyObject{
    var isDataLoading:Bool {get}
    var selectedNycSchool: NYCSchool {get}
    var satScore: SATScore? {get}
    func loadSATScores()
    func navigateToWebsite()
    func mailTo()
    func makeACall()
}

class DetailViewModel: NSObject, DetailViewModelProtocol{
    
    var isDataLoading:Bool = false
    var selectedNycSchool: NYCSchool
    var satScore: SATScore?
    weak var delegate: DetailViewModelDelegate?
    
    init(selectedNycSchool: NYCSchool, delegate: DetailViewModelDelegate) {
        self.selectedNycSchool = selectedNycSchool
        self.delegate = delegate
    }
    
    func loadSATScores() {
        if(!isDataLoading){
            self.delegate?.showOrHideLoadingIndicator(shouldShowLoadingIndicator: true)
            NetworkManager.shared.getData(endpoint: .detail, queryItems: [URLQueryItem(name: "dbn", value: selectedNycSchool.Id)], type: [SATScore].self){ [weak self] score, error in
                if let self = self{
                    self.isDataLoading = false
                    if error != nil{
                        self.delegate?.showOrHideLoadingIndicator(shouldShowLoadingIndicator: false)
                    }
                    guard let score = score else {
                        self.delegate?.showOrHideLoadingIndicator(shouldShowLoadingIndicator: false)
                        return
                    }
                    if !score.isEmpty{
                        self.satScore = score.first!
                    }
                    self.delegate?.showOrHideLoadingIndicator(shouldShowLoadingIndicator: false)
                }
                
            }
        }
    }
    
    func navigateToWebsite() {
        if let website = selectedNycSchool.website{
            OpenUrlService.shared.navigateToWebsite(website)
        }
    }
    
    func mailTo() {
        if let email = selectedNycSchool.email{
            OpenUrlService.shared.sendMail(email)
        }
    }
    
    func makeACall() {
        if let phoneNumber = selectedNycSchool.phoneNumber{
            OpenUrlService.shared.makeACall(phoneNumber)
        }
    }
    
}
